const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.addReportsTitles = functions.database.ref(`/reports/{reportKey}`)
    .onCreate(event => {
        const key = event.data.key;
        const value = event.data.val();
        value.$key = key;
        console.log(value.$key);
        //admin.database().ref(`/reports-titles/${key}`).set({title: value.title});
        admin.database().ref(`/reports/${key}`).set(value);
    })