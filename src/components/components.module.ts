import { SendUpdateComponent } from './send-update/send-update';
import { ReportComponent } from './report/report';
import { ReportsComponent } from './reports/reports';
import { NewReportComponent } from './new-report/new-report';
import { ProfileViewComponent } from './profile-view/profile-view';
import { EditProfileFormComponent } from './edit-profile-form/edit-profile-form';
import { RegisterFormComponent } from './register-form/register-form';
import { LoginFormComponent } from './login-form/login-form';
import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
  declarations: [
      LoginFormComponent,
      RegisterFormComponent,
      EditProfileFormComponent,
      ProfileViewComponent,
      NewReportComponent,
      ReportsComponent,
      ReportComponent,
      SendUpdateComponent
  ],
  imports: [
    IonicModule
  ],
  exports: [
      LoginFormComponent,
      RegisterFormComponent,
      EditProfileFormComponent,
      ProfileViewComponent,
      NewReportComponent,
      ReportsComponent,
      ReportComponent,
      SendUpdateComponent
  ]
})

export class ComponentsModule {

}