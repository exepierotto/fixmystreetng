import { Upload } from './../../models/upload/upload.interface';
import { User } from 'firebase/app';
import { AuthService } from './../../providers/auth/auth.service';
import { DataService } from './../../providers/data/data.service';
import { Profile } from './../../models/profile/profile.interface';
import { Component, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { NavController, NavParams } from "ionic-angular";

@Component({
  selector: 'edit-profile-form',
  templateUrl: 'edit-profile-form.html'
})
export class EditProfileFormComponent implements OnDestroy {

  private authenticadedUser$: Subscription;
  authenticadedUser: User;

  avatar: File;
  upload: Upload;

  @Output() saveProfileResult: EventEmitter<Boolean>;

  profile = {} as Profile;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private auth: AuthService,
    private data: DataService) {
    this.saveProfileResult = new EventEmitter<Boolean>();
    this.authenticadedUser$ = this.auth.getAuthState().subscribe((user: User) => {
      this.authenticadedUser = user;
    })
  }

  ionViewWillEnter(){
    this.profile = this.navParams.get('profile');
    console.log(this.profile);
  }

  getFileSelected(event) {
    this.avatar = event.target.files.item(0);
      console.log('GetFile');
      console.log(this.avatar);
  }

  fileUpload() {
    this.upload = new Upload(this.avatar);
    this.upload.path = `/avatars/${this.authenticadedUser.uid}`;
    this.upload.name = this.authenticadedUser.uid;
    this.data.uploadAvatar(this.upload, this.profile);
  }

  async saveProfile() {
    if (this.authenticadedUser) {
      if (this.avatar) {
        await this.fileUpload();
      }
      console.log('SaveProfile');
      console.log(this.profile);
      this.profile.email = this.authenticadedUser.email;
      const result = await this.data.saveProfile(this.authenticadedUser.uid, this.profile);
      console.log('SaveResult');
      console.log(result);
      this.saveProfileResult.emit(result);
    } else {
      console.log('Error while saving profile')
    }

  }

  cancel() {
    this.navCtrl.setRoot('ReportsPage')
  }

  ngOnDestroy(): void {
    this.authenticadedUser$.unsubscribe();
  }
}
