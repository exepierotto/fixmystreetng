import { AuthService } from './../../providers/auth/auth.service';
import { LoginResponse } from './../../models/login/login-response.interface';
import { Account } from './../../models/account/account.interface';
import { NavController } from 'ionic-angular';
import { Component, EventEmitter, Output } from '@angular/core';

/**
 * Generated class for the LoginFormComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'login-form',
  templateUrl: 'login-form.html'
})
export class LoginFormComponent {

  account = {} as Account;

  @Output() loginStatus: EventEmitter<LoginResponse>;

  constructor(private auth: AuthService ,private navCtrl: NavController) {
    this.loginStatus = new EventEmitter<LoginResponse>();
  }

  async signInWithEmailAndPassword() {
    const loginResponse: LoginResponse = await this.auth.signInWithEmailAndPassword(this.account);
    this.loginStatus.emit(loginResponse);
    console.log(loginResponse);
  }

  async signInAnonymously() {
    const loginResponse: LoginResponse = await this.auth.signInAnonymously();
    this.loginStatus.emit(loginResponse);
    console.log(loginResponse);
  }
  
  goToRegisterPage() {
    this.navCtrl.push('RegisterPage');
  }
}