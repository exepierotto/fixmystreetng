import { Subscription } from 'rxjs/Subscription';
import { User } from 'firebase/app';
import { AuthService } from './../../providers/auth/auth.service';
import { ReportService } from './../../providers/report/report.service';
import { Report } from './../../models/reports/report.interface';
import { Component, Output, EventEmitter } from '@angular/core';
import { LoadingController, Loading, NavController } from "ionic-angular";
import { Upload } from "../../models/upload/upload.interface";

/**
 * Generated class for the NewReportComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'new-report',
  templateUrl: 'new-report.html'
})
export class NewReportComponent {

  private authenticadedUser$: Subscription;
  authenticadedUser: User;
  report = {} as Report;
  upload: Upload;
  loader: Loading;

  @Output() newReportResult: EventEmitter<Boolean>;

  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
    private loading: LoadingController,
    private reportService: ReportService) {
    this.newReportResult = new EventEmitter<Boolean>();
    this.authenticadedUser$ = this.auth.getAuthState().subscribe((user: User) => {
      this.authenticadedUser = user;
    });
    this.loader = this.loading.create({
      content: 'Creating Report...'
    });
  }

  getFileSelected(event) {
    this.upload = new Upload(event.target.files.item(0));
    console.log('GetFile');
  }

  createReport() {
    if (this.authenticadedUser) {
      this.loader.present();
      this.report.date = new Date().getTime();
      this.report.user = this.authenticadedUser.uid;
      console.log('New Report');
      console.log(this.report);
      this.reportService.newReport(this.report, this.upload);
      this.newReportResult.emit(true);
      this.loader.dismiss();
    } else {
      this.navCtrl.setRoot('LoginPage');
    }
  }

  ngOnDestroy(): void {
    this.authenticadedUser$.unsubscribe();
  }
}