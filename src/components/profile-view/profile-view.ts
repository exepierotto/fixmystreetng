import { User } from 'firebase/app';
import { DataService } from './../../providers/data/data.service';
import { AuthService } from './../../providers/auth/auth.service';
import { Profile } from './../../models/profile/profile.interface';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoadingController, Loading, NavController } from "ionic-angular";

/**
 * Generated class for the ProfileViewComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'profile-view',
  templateUrl: 'profile-view.html'
})
export class ProfileViewComponent implements OnInit {

  userProfile: Profile;
  loader: Loading;

  @Output() existingProfile: EventEmitter<Profile>;

  constructor(
    private navCtrl: NavController,
    private loading: LoadingController,
    private auth: AuthService,
    private data: DataService) {
    
    this.existingProfile = new EventEmitter<Profile>();
    this.loader = this.loading.create({
      content: 'Loading Profile...'
    })
  }

  ngOnInit(): void {
    this.loader.present();

    this.data.getAuthenticadedUserProfile().subscribe(profile => {
      this.userProfile = profile;
      console.log(this.userProfile);
      this.existingProfile.emit(this.userProfile);
      this.loader.dismiss();
    })

  }

  goToEditProfilePage(profile: Profile){
    this.navCtrl.push('EditProfilePage', { profile });
  }
  
  logOut() {
    console.log('EXIT');
  }
}