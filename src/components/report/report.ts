import { Component } from '@angular/core';

/**
 * Generated class for the ReportComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'report',
  templateUrl: 'report.html'
})
export class ReportComponent {

  text: string;

  constructor() {
    console.log('Hello ReportComponent Component');
    this.text = 'Hello World';
  }

}
