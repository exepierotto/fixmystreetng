import { NavController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2/database';
import { Report } from './../../models/reports/report.interface';
import { ReportService } from './../../providers/report/report.service';
import { Component } from '@angular/core';

/**
 * Generated class for the ReportsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'reports',
  templateUrl: 'reports.html'
})
export class ReportsComponent {

  reportsList: FirebaseListObservable<Report[]>

  constructor(private report: ReportService, private navCtrl: NavController) {
    this.getReports();
  }

  getReports(){
    this.reportsList = this.report.getReports();
  }

  openReport(report: Report){
    this.navCtrl.push('ReportPage', { report });
  }
}
