import { ReportUpdate } from './../../models/reports/report-update.interface';
import { Component, Output, EventEmitter } from '@angular/core';
import { AuthService } from "../../providers/auth/auth.service";
import { User } from "firebase/app";
import { Subscription } from "rxjs/Subscription";
import { NavController } from "ionic-angular";

/**
 * Generated class for the SendUpdateComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'send-update',
  templateUrl: 'send-update.html'
})
export class SendUpdateComponent {

  @Output() postUpdate: EventEmitter<ReportUpdate>;

  update = {} as ReportUpdate;
  private authenticadedUser$: Subscription;
  authenticadedUser: User;

  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
  ) {
    this.postUpdate = new EventEmitter<ReportUpdate>();
    this.authenticadedUser$ = this.auth.getAuthState().subscribe((user: User) => {
      this.authenticadedUser = user;
    });
  }

  post() {
    if (this.authenticadedUser) {
      this.update.date = new Date().getTime();
      this.update.user = this.authenticadedUser.uid;
      this.postUpdate.emit(this.update);
      this.update.message = "";
    } else {
      this.navCtrl.setRoot('LoginPage');
    }
  }

  ngOnDestroy(): void {
    this.authenticadedUser$.unsubscribe();
  }
}
