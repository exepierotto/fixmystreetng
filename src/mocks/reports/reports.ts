import { Profile } from './../../models/profile/profile.interface';
import { USER_LIST } from './../users/users';
import { Report } from './../../models/reports/report.interface';

const userList: Profile[] = USER_LIST;
const reportList: Report[] = [];

userList.forEach( user => {
    reportList.push({user: 'user', date: new Date().getTime(), title: "Report " + userList.indexOf(user), description: 'A description', category: 'category', location: 'Somewhere'})
})

export const REPORT_LIST = reportList;