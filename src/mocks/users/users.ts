import { Profile } from './../../models/profile/profile.interface';

const userList: Profile[] = [
    { name: 'John', surname: 'Rambo', email: 'john.rambo@movie.com', avatar: './../../assets/img/rambo.png'},
    { name: 'Bruce', surname: 'Lee', email: 'bruce.lee@movie.com', avatar: './../../assets/img/bruce.png'},
    { name: 'Duck', surname: 'Lucas', email: 'duck.lucas@movie.com', avatar: './../../assets/img/lucas.png'},
    { name: 'Bugs', surname: 'Bunny', email: 'bugs.bunny@movie.com', avatar: './../../assets/img/bugs.png'}
]

export const USER_LIST = userList;