export interface Profile {
    $key?: string;
    name?: string;
    surname?: string;
    avatar?: string;
    email?: string;
}