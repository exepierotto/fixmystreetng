export interface ReportUpdate {
    $key?: string;
    message: string;
    user?: string;
    date?: number;
}