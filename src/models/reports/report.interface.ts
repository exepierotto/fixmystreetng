export interface Report {
    $key?: string;
    user?: string;
    date?: number;
    imageUrl?: string;
    title: string;
    description?: string;
    category?: string;
    location?: string;
}