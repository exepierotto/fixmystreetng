export class Upload {
    file: File;
    progress: number;
    url: string;
    path: string;
    name: string;

   constructor(file: File) {
    this.file = file; 
   }
} 