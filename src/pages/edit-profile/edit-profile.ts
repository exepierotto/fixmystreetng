import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the EditProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  constructor(
    private toast: ToastController,
    private navCtrl: NavController,
    private navParams: NavParams) {
  }

  saveProfileResult(event: boolean) {
    if (event) {
      this.toast.create({
        message: 'Profile updated',
        duration: 3000
      }).present();
      this.navCtrl.setRoot('ReportsPage')
    } else {
      this.toast.create({
        message: 'Please login again',
        duration: 3000
      }).present();
      this.navCtrl.setRoot('LoginPage')
    }
  }

}
