import { User } from 'firebase/app';
import { DataService } from './../../providers/data/data.service';
import { LoginResponse } from './../../models/login/login-response.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    private data: DataService,
    private toast: ToastController,
    private navCtrl: NavController) {
  }

  login(event: LoginResponse) {
    console.log(event);
    if (!event.error) {
      this.toast.create({
        message: 'Login successful',
        duration: 3000
      }).present();

      this.data.getProfile(event.result.uid).subscribe( profile =>{
        console.log(profile);
        if (profile.val()) {
          this.navCtrl.setRoot('ReportsPage');
        } else {
          this.navCtrl.setRoot('EditProfilePage');
        }
      })
    } else {
      this.toast.create({
        message: event.error.message,
        duration: 3000
      }).present();
    }
  }
}
