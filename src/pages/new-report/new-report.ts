import { ReportService } from './../../providers/report/report.service';
import { Report } from './../../models/reports/report.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the NewReportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-new-report',
  templateUrl: 'new-report.html',
})
export class NewReportPage {

  constructor(
    private toast: ToastController,
    private navCtrl: NavController,
    private navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewReportPage');
  }

  reportResult(event: boolean) {
    if (event) {
      this.toast.create({
        message: 'Report created',
        duration: 3000
      }).present();
      this.navCtrl.setRoot('ReportsPage')
    } else {
      this.toast.create({
        message: 'Please try again',
        duration: 3000
      }).present();
    }
  }
}
