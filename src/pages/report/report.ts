import { ReportUpdate } from './../../models/reports/report-update.interface';
import { FirebaseListObservable } from 'angularfire2/database';
import { ReportService } from './../../providers/report/report.service';
import { Report } from './../../models/reports/report.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {

  report: Report;
  reportUpdates: FirebaseListObservable<ReportUpdate[]>;

  constructor(
    private reportService: ReportService,
    private navCtrl: NavController,
    private navParams: NavParams) {
  }

  ionViewWillEnter(){
    this.report = this.navParams.get('report');
    this.reportUpdates = this.reportService.getReportUpdates(this.report.$key);
    console.log(this.report);
  }

  post(update: ReportUpdate) {
    console.log(update);
    console.log(this.report.$key);
    this.reportService.postReportUpdate(this.report.$key, update);
  }
}
