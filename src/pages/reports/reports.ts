//import { Report } from './../../models/reports/report.interface';
//import { REPORT_LIST } from './../../mocks/reports/reports';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReportsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToProfilePage() {
    this.navCtrl.push('ProfilePage');
  }

  goToNewReportPage() {
    this.navCtrl.push('NewReportPage');
  }
}
