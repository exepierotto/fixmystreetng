import { LoginResponse } from './../../models/login/login-response.interface';
import { Account } from './../../models/account/account.interface';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor(private auth: AngularFireAuth) {
    console.log('Hello AuthProvider Provider');
  }

  getAuthState(){
    return this.auth.authState;
  }

  async createUserWithEmailAndPassword(account: Account) {
    try {
      return <LoginResponse> { 
        result: await this.auth.auth.createUserWithEmailAndPassword(account.email, account.password)
      }
    } catch (error) {
      return <LoginResponse> { 
        error: error
      }
    }
  }

  async signInWithEmailAndPassword(account: Account) {
    try {
      return <LoginResponse> { 
        result: await this.auth.auth.signInWithEmailAndPassword(account.email, account.password)
      }
    } catch (error) {
      return <LoginResponse> { 
        error: error
      }
    }
  }

  async signInAnonymously() {
    try {
      return <LoginResponse> { 
        result: await this.auth.auth.signInAnonymously()
      }
    } catch (error) {
      return <LoginResponse> { 
        error: error
      }
    }
  }

  signOut() {
    this.auth.auth.signOut();
  }
}
