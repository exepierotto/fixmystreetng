import { Upload } from './../../models/upload/upload.interface';
import { AuthService } from './../auth/auth.service';
import { Report } from './../../models/reports/report.interface';
import { Profile } from './../../models/profile/profile.interface';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from "angularfire2/database";
import { User } from "firebase/app";
import * as firebase from "firebase";
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class DataService {

  profileObject: FirebaseObjectObservable<Profile>
  reportList: FirebaseListObservable<Report>

  constructor(
    private authService: AuthService,
    private database: AngularFireDatabase) {
    console.log('Hello DataProvider Provider');
  }

  getAuthenticadedUserProfile() {
    return this.authService.getAuthState()
      .map(user => user.uid)
      .mergeMap(authId => this.database.object(`/profiles/${authId}`))
      .take(1)
  }

  getProfile(user: string) {
    this.profileObject = this.database.object(`/profiles/${user}`, { preserveSnapshot: true });
    return this.profileObject.take(1);
  }

  async saveProfile(user: string, profile: Profile) {
    this.profileObject = this.database.object(`/profiles/${user}`);
    try {
      await this.profileObject.set(profile);
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  async uploadAvatar(upload: Upload, profile: Profile) {
    let storageRef = firebase.storage().ref().child(upload.path);
    storageRef.put(upload.file).then(
      (onFullfilled) => {
        console.log('UPLOAD-SNAPSHOT');
      }
    ).then(() => {
      storageRef.getDownloadURL().then((url) => {
        console.log('UPLOAD-URL');
        console.log(url);
        upload.url = url;
        console.log(upload);
      }).then(()=>{
        profile.avatar = upload.url;
        console.log('UPLOAD-PROFILE');
        console.log(profile);
        this.saveProfile(upload.name, profile);
      });
    })
  }

  async uploadImageReport(upload: Upload, profile: Profile) {
    let storageRef = firebase.storage().ref().child(upload.path);
    storageRef.put(upload.file).then(
      (onFullfilled) => {
        console.log('UPLOAD-SNAPSHOT');
      }
    ).then(() => {
      storageRef.getDownloadURL().then((url) => {
        console.log('UPLOAD-URL');
        console.log(url);
        upload.url = url;
        console.log(upload);
      }).then(()=>{
        profile.avatar = upload.url;
        console.log('UPLOAD-PROFILE');
        console.log(profile);
        this.saveProfile(upload.name, profile);
      });
    })
  }

  async uploadImageReportUpdate(upload: Upload, profile: Profile) {
    let storageRef = firebase.storage().ref().child(upload.path);
    storageRef.put(upload.file).then(
      (onFullfilled) => {
        console.log('UPLOAD-SNAPSHOT');
      }
    ).then(() => {
      storageRef.getDownloadURL().then((url) => {
        console.log('UPLOAD-URL');
        console.log(url);
        upload.url = url;
        console.log(upload);
      }).then(()=>{
        profile.avatar = upload.url;
        console.log('UPLOAD-PROFILE');
        console.log(profile);
        this.saveProfile(upload.name, profile);
      });
    })
  }

  searchReport(title: string) {
    const query = this.database.list('/reports', {
      query: {
        orderByChild: 'title',
        equalTo: title
      }
    })

    return query.take(1);
  }

}
