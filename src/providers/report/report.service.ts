import { Upload } from './../../models/upload/upload.interface';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { ReportUpdate } from './../../models/reports/report-update.interface';
import { FirebaseListObservable } from 'angularfire2/database';
import { Report } from './../../models/reports/report.interface';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from "firebase";
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ReportProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ReportService {

  constructor(
    private database: AngularFireDatabase) {
    console.log('Hello ReportProvider Provider');
  }

  newReport(report: Report, upload: Upload) {
    let key = this.database.list(`/reports/`).push(report).key;

    if (upload.file) {
      upload.path = `/reports/${key}`;

      let storageRef = firebase.storage().ref().child(upload.path);
      storageRef.put(upload.file).then(
        (onFullfilled) => {
          console.log('UPLOAD-SNAPSHOT');
        }
      ).then(() => {
        storageRef.getDownloadURL().then((url) => {
          console.log('UPLOAD-URL');
          console.log(url);
          report.imageUrl = url;
          console.log(upload);
        }).then(() => {
          console.log('UPLOAD-REPORT');
          this.updateReport(key, report);
        });
      })
    }
  }

  getReports(): FirebaseListObservable<Report[]> {
    return this.database.list(`/reports/`);
  }

  getReport(key: string): FirebaseObjectObservable<Report> {
    return this.database.object(`/reports/${key}`);
  }

  getReportsTitles(): FirebaseListObservable<Report[]> {
    return this.database.list(`/reports-titles/`);
  }

  getReportUpdates(key: string) {
    console.log(key)
    return this.database.list(`/reports-updates/${key}`);
  }

  async updateReport(key: string, report: Report) {
    console.log(key)
    await this.database.list(`/reports/`).update(key, report);
  }

  async postReportUpdate(key: string, update: ReportUpdate) {
    console.log(key)
    await this.database.list(`/reports-updates/${key}/`).push(update);
  }
}